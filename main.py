import eventlet
import socketio

sio = socketio.Server(async_mode='eventlet', cors_allowed_origins='*')

TEST_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAAXNSR0IB2cksfwAAAH5QTFRFAAAA4ODgt6+5cVp1XEFhb1l0tKu2Z09s/KRY/5xF+6RacVt2/61h+cOa/+W1/9mk/7Np5lxJ6nJa+LOE6nNbxFVPtay3ckVduFJRv7jAc0VdnWplm2dk96Nf/9mj/7Rq/7Jn6nFZ+LeM6GlScURcu1NQckVcaUNew73EmmdkuyH09wAAACp0Uk5TAP//////////////////////////////////////////////////////J0/sFAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAV5JREFUeJzt2ctSwkAQhWHUoEaIGhQVMeBdef8XdCGlMM6pKTOdns1/lqmumm+VOemMDgpnBAAAAAAAAAAAAAAAAAAAAAAAAACAeH54VJlkfNwTcHJam+Rs0hNQ2Zxf1xUAAH0B09KA5jyWi0s3QBvN7KowoL0uDWi9AJN5YcDNOHq33N55AUQW96UBS29A0AseOm9Adi/IBWTfCQAAZANye0E2YL8XrNbqoMdVfC4bsH8VPck+8DyLzxkDdB94EXPWAHkdq7lcwJ9e4A0IesGr7ANv7/E5tz7w8Rmfc+sDwUGVNWDbC3QfGBqQ7AVDA5J3AgAAgwNSvWC52B3/fV+YAZpEH+g2u6/iTWcO+L5i/PYDAuC3H1AAt8/zMD+9oBRg2wvYD/z7O2FqDojvD2Uac0BYPlMBYA2Q+0ORed9fNipif2j/08otAAAAAAAAAAAAAAAAAAAAAAAAAAB8AczYs2A5g3z+AAAAAElFTkSuQmCC'

with open("audiofile.m4a", mode='rb') as file:
    TEST_AUDIO = []
    b = bytearray(file.read())
    for i in range(len(b)):
        TEST_AUDIO.append(int(b[i]))

print(TEST_AUDIO[-1])


@sio.event
def connect(sid, environ):
    sio.emit('testimage', {'image': TEST_IMAGE})
    sio.emit('testaudio', {'audio': TEST_AUDIO})


app = socketio.WSGIApp(sio)
eventlet.wsgi.server(eventlet.listen(('0.0.0.0', 5000)), app)
